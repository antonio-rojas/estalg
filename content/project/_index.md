+++
title = "Proyecto docente"
weight = 30
pre = "<i class='fa fa-book'></i> "
+++

{{< button href="../docs/project.pdf" align="center" >}} Documento oficial {{< /button >}}

### ¿Qué vamos a estudiar?

La asignatura se centrará en el estudio de los conceptos de anillo, módulo y cuerpo y sus aplicaciones a las ecuaciones diofánticas, los operadores lineales, las construcciones geométricas con regla y compás y las ecuaciones polinómicas de grado superior en una variable. 

Las presentes notas son lo suficienemente completas como para seguir la asignatura. Estas notas se basan en los capítulos correspondientes de la **segunda edición** del libro [**Algebra**](https://fama.us.es/discovery/search?query=any,contains,algebra%20artin&tab=all_data_not_idus&search_scope=all_data_not_idus&sortby=date_d&vid=34CBUA_US:VU1&facet=frbrgroupid,include,20288007660796345&lang=es&offset=0), de Michael Artin.

![book](../images/book.jpeg)

Concretamente usaremos los siguientes capítulos y secciones:

* Capítulo 11, secciones 5, 7 y 8.

* Capítulo 12, secciones 2, 3 y 5.

* Capítulo 14, todas las secciones excepto 3, 6 y 9.

* Capítulo 15, todas las secciones excepto 6, 7, 9 y 10.

* Capítulo 16, todas las secciones excepto 8 y 9.

En el último además veremos parte de la sección 5 del capítulo 7.

Recomendamos repasar conceptos vistos en Álgebra Básica que se corresponden con los siguientes capítulos y secciones del libro:

* Capítulo 1, sección 5.

* Capítulo 2.

* Capítulo 7, sección 1.

* Capítulo 11, secciones de la 1, 2, 3, 4 y 6.

* Capítulo 12, secciones 1, 3 y 4.

### Clases

Cuatro horas semanales, de las cuales una hora será de problemas, de media a lo largo del semestre. En el siguiente enlace podéis descargar la lista de ejercicios y problemas que haremos en clase:

{{< button href="../docs/ejercicios.pdf" align="center" >}} Problemas {{< /button >}}


### ¿Cómo vamos a evaluar?

La evaluación continua se basará en dos exámenes. El primero, que tratará sobre anillos y módulos, se realizará previsiblemente a pricipios de noviembre y el segundo, sobre cuerpos, hacia el final del cuatrimestre. Las fechas precisas y el carácter presencial o en línea de cada examen se fijarán con la suficiente antelación. La nota final de la evaluación continua será la media aritmética de ambos exámenes. Los estudiantes que no aprueben por este método podrán presentarse a las convocatorias oficiales en las fechas designadas por la Facultad de Matemáticas bajo las condiciones establecidas por la Universidad de Sevilla. 

Quien apruebe solo uno de los dos exámenes de la evaluación continua y tenga una media suspensa podrá, si así lo desea, presentarse al examen de la primera convocatoria y examinarse solo de la parte que suspendió. Su nota final será la media aritmética de la parte que aprobó y de la que se examine en la primera convocatoria. También podrán presentarse a la primera convocatoria aquellos estudiantes aprobados que deseen subir su nota, en ningún caso la bajarán.

### Tutorías

En la [Enseñanza Virtual](https://ev.us.es) hemos habilitado un foro donde los estudiantes podrán plantear sus dudas sobre los contenidos de la asignatura. Los profesores lo leeremos con regularidad y las iremos respondiendo. 

El horario de tutorías del profesorado puede encontrarse en [la página del Departamento de Álgebra](http://www.algebra.us.es/). Si quieres concertar una cita puedes escribir a las siguientes direcciones.

| Profesores                                                   | Correo             |
| ------------------------------------------------------------ | ------------------ |
| [Sara Arias-de-Reyna](https://personal.us.es/sara_arias/)    | <sara_arias@us.es> |
| [Luis Narváez](https://personal.us.es/narvaez/)              | <narvaez@us.es>      |
| [Antonio Rojas](https://personal.us.es/arojas/) (coordinador)| <arojas@us.es>     |
| Jesús Soto                                                   | <soto@us.es>       |
